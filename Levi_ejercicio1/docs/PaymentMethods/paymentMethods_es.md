# Payment Methods en Cuentas y Tarjetas

## Qu� son los Payment Methods

Los _Payment methods_ hacen referencia al pago del cr�dito consumido en cuentas y tarjetas de cr�dito. B�sicamente, se refieren a c�mo el cliente tiene configurado la devoluci�n del cr�dito que ha consumido. Cada cuenta y tarjeta de cr�dito tiene su propia configuraci�n.

## Configuraci�n de un Payment Method

Las variables que determinan el m�todo de pago de una cuenta/tarjeta de cr�dito son las siguientes:

   * **Tipo de pago**: Identifica la forma de pago seleccionada respecto de la cantidad total del cr�dito consumido: pago total, m�nimo permitido, porcentaje...
   * **Periodicidad**: Hace referencia a la frecuencia con que se realizar� el pago: mensual, quincenal...
   * **Cantidad a pagar**: Indica la cantidad que se va a pagar,
   * **Fecha l�mite**: Marca el �ltimo d�a posible para efectuar el siguiente pago de la deuda mediante el m�todo seleccionado. Si no se realiza, el cliente entra en impago, pudiendo generar intereses de demora y/o suponer el bloqueo de la cuenta/tarjeta temporal o definitivo.
      + La fecha l�mite est� marcada por la periodicidad: mensual => todos los d�as 5 de cada mes; anual => todos los d�as 25 de diciembre...

### Tipos de pago

Actualmente, los tipos de pago que se contemplan son:

   * **Total de la deuda**: La cantidad que se paga es el total del cr�dito consumido a la fecha de corte.
   * **M�nimo**: La cantidad que se paga es la m�nima exigida por el banco respecto al cr�dito consumido a la fecha de corte, habitualmente, para poder seguir operando con la cuenta/tarjeta.
   * **M�nimo para evitar intereses**: La cantidad que se paga es la m�nima exigida por el banco respecto al cr�dito consumido a la fecha de corte para evitar intereses de demora.
   * **Cantidad fija**: La cantidad que se paga es una cantidad fija.
   * **Porcentaje fijo**: La cantidad que se paga es un un porcentaje fijo respecto del cr�dito consumido a la fecha de corte.
   * **Cantidad libre**: La cantidad que se paga es decidida por el usuario al momento de efectuar el pago.
   
De estas opciones, habr� algunas (o todas) disponibles dependiendo del tipo de producto, del producto comercial y de las consideraciones locales.
   
### Fecha de corte y cantidades de pago

La _fecha de corte_ marca el �ltimo d�a en el que las operaciones realizadas con la cuenta/tarjeta de cr�dito se tienen en cuenta para el siguiente pago. Al igual que la _fecha l�mite_, esta fecha viene determinada por la periodicidad.

Al llegar a esa fecha, se fijan una serie de cantidades de referencia en base a las cuales el cliente puede configurar su m�todo de pago. Estas cantidades son:

   * **Deuda total**: Coincide con todo el cr�dito consumido hasta la fecha de corte, incluidos pagos financiados. Este valor coincide con el _balance dispuesto real_ de la cuenta/tarjeta a la fecha de corte.
   * **M�nimo**: Determina la cantidad m�nima a pagar en el siguiente pago exigida por el banco para no caer en impago.
   * **M�nimo para evitar intereses**: Determina la cantidad m�nima a pagar el siguiente pago exigida por el banco para no aplicar intereses de demora.
   * **Consumo del �ltimo periodo**: Indica la cantidad consumida durante el �ltimo periodo, incluyendo los plazos a pagar de pagos financiados.
   
### Ejemplo de Payment Methods

En el siguiente ejemplo, se muestra c�mo vendr�an informados los conceptos anteriores en funci�n del consumo realizado con una tarjeta de cr�dito:

   * **Fecha: 01/01/2017**
      + Tenemos una tarjeta con un cr�dito concedido de 3.000� donde:
         * La _periodicidad_ es _mensual_
	     * La _fecha de corte_ se fija el d�a 25 de cada mes
		    + La siguiente fecha de corte es el 25/01/2017
	     * La _fecha l�mite_ se fija el d�a 28 de cada mes
		    + La siguiente fecha l�mite es el 28/01/2017
   * **Fecha: 26/01/2017**
      + El cliente ha acumulado un gasto hasta el d�a anterior de 2.000�, quedando los valores de la siguiente manera:
         * _M�nimo_ (supongamos que el banco lo fija en el 15% de la deuda) -> 300�
         * _M�nimo para evitar intereses_ (supongamos que el banco lo fija en el 50% de la deuda) -> 1.000�
	     * _Consumo del �ltimo periodo_ -> 2.000�
         * _Deuda total_ -> 2.000�
	  + El cliente decide pagar 1.200�
	     * Se arrastra para el siguiente periodo un consumo de 800�
   * **Fecha: 01/02/2017**
      + Las fechas se actualizan de la siguiente manera:
		 * La siguiente fecha de corte es el 25/02/2017
	     * La siguiente fecha l�mite es el 28/02/2017
   * **Fecha: 26/02/2017**
      + El cliente, desde la anterior fecha de corte hasta el d�a de ayer, ha realizado un gasto de 750�, quedando los valores de la siguiente manera:
         * _M�nimo_ (15% de la deuda) -> 232.5�
         * _M�nimo para evitar intereses_ (50% de la deuda) -> 775�
	     * _Consumo del �ltimo periodo_ -> 750�
	     * _Deuda total_ -> 1.550� (750� del gasto de este periodo + 800� arrastrados del periodo anterior)
   
## Estructura del recurso Payment Method

A continuaci�n, se explica c�mo los conceptos funcionales se muestran en la estructura de Payment Methods:

   * **id**: El identificador del Payment Method indica el _tipo de pago_ seleccionado. La correspondencia entre _tipos de pago_ e _ids_ es la siguiente:
      + **TOTAL_AMOUNT_PAYMENT**: Este id hace referencia al tipo de pago _Total de la deuda_
      + **MINIMUM_AMOUNT_PAYMENT**: Este id hace referencia al tipo de pago _M�nimo_
      + **MINIMUM_AMOUNT_TO_AVOID_INTEREST_PAYMENT**: Este id hace referencia al tipo de pago _M�nimo para evitar intereses_
      + **FIXED_AMOUNT_PAYMENT**: Este id hace referencia al tipo de pago _Cantidad fija_
      + **FIXED_PERCENTAGE_PAYMENT**: Este id hace referencia al tipo de pago _Porcentaje fijo_
      + **FREE_AMOUNT_PAYMENT**: Este id hace referencia al tipo de pago _Cantidad libre_
   * **period**: Se corresponde con la _Periodicidad_ con los siguientes posibles valores
   * **monetaryPayments**: Este atributo es un array que se corresponde con la _Cantidad a pagar_ y se informa cuando el tipo de pago seleccionado es _TOTAL_AMOUNT_PAYMENT_, _MINIMUM_AMOUNT_PAYMENT_,  _MINIMUM_AMOUNT_TO_AVOID_INTEREST_PAYMENT_ o _FIXED_AMOUNT_PAYMENT_
      + La estructura de array permite cubrir el doble cr�dito de las tarjetas de cr�dito de Chile.
   * **percentage**: Este atributo se corresponde con la _Cantidad a pagar_ y se informa s�lo cuando el tipo de pago seleccionado es _FIXED_PERCENTAGE_PAYMENT_
   * **endDate**: Esta fecha indica la _Fecha l�mite_ para realizar el siguiente pago. Esta fecha se actualiza conforme a la periodicidad fijada.
   * **paymentAmounts**: Este array informa de las cantidades de referencia disponibles en base al cr�dito consumido hasta la fecha de corte.
      + El atributo **id** indica de qu� cantidad se trata:
	     * **MINIMUM_AMOUNT** Indica que la cantidad es el _M�nimo_
		 * **MINIMUM_AMOUNT_TO_AVOID_INTEREST** Indica que la cantidad es el _M�nimo para evitar intereses_
		 * **TOTAL_DEBT_AMOUNT** Indica que la cantidad es la _Deuda total_
		 * **LAST_PERIOD_AMOUNT** Indica que la cantidad es el _Consumo del �ltimo periodo_
      + El array **values** indica el valor monetario
	     + La estructura de array permite cubrir el doble cr�dito de las tarjetas de cr�dito de Chile.
		 
Por �ltimo, la _fecha de corte_ no est� dentro de esta estructura, sino del detalle de la cuenta/tarjeta en el atributo **cutoffDate**.