# Blocks
A block resource represents the reason for which an account is blocked, either by the government for a political, judicial or some other reason, like the death of the account holder.

A block does not just freeze an amount of money, it blocks the account itself. If the account is blocked, the user will not be able to perform any operation with it.

## How to Block an Account
The service endpoint that MUST be used to create a block on an account is `POST /accounts/{account-id}/blocks`.
 
Additionally, this action MUST trigger any other resource state changes related to the blocking process in the backend (like changing its `status` to `BLOCKED`).
 
## Account Block Anti-Patterns
There are other endpoints from which you could apparently obtain the same end-result, these are considered anti-patterns for blocking an account.
 
### Through Activations
Since multiple activations can be off for an account at a specific time, a client could turn both activations: CREDIT_OPERATIONS and DEBIT_OPERATIONS off, effectively blocking most operations on it.
 
This MUST NOT be done, because of 2 reasons:

 1. The functional purpose of an activation is to be done on the account holder's demand, whereas a block is usually done by government order.
 2. The status of the account does not change with an activation, its operation is only constrained.
  
### Calling PATCH on the account and modifying its status directly
Calling PATCH to modify the account status should only be done to modify general data related to the account contract and other statuses.
  
This is because all APIs become simpler to consume if there is a clear separation between actions that only modify general data on a product and actions that affect its operation.